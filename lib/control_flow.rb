# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char do |ch|
    str.delete!(ch) if ch == ch.downcase
  end
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.even?
    str[str.length/2 - 1] + str[str.length/2]
  else
    str[str.length/2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.each_char do |ch|
    count += 1 if VOWELS.include?(ch.downcase)
  end
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  count = 1
  (1..num).each do |i|
    count *= i
  end
  count
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  string = ""
  i = 0
  return string if arr.empty?
  while i < arr.length - 1
    string << arr[i] + separator
    i += 1
  end
  string << arr[-1]
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  string = ""
  arr = str.chars
  arr.each_index do |idx|
    if (idx + 1).odd?
      string << arr[idx].downcase
    else
      string << arr[idx].upcase
    end
  end
  string
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  arr = str.split
  arr.map do |word|
    if word.length >= 5
      word.reverse!
    end
  end
  arr.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  arr = []
  (1..n).each do |num|
    if num % 3 == 0 && num % 5 == 0
      arr << "fizzbuzz"
    elsif num % 3 == 0
      arr << "fizz"
    elsif num % 5 == 0
      arr << "buzz"
    else
      arr << num
    end
  end
  arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  count = 0

  if num > 2
    (1..num).each do |i|
      if num % i == 0
        count += 1
      end
    end
  elsif num == 1
    return false
  end

  if count > 2
    return false
  else
    true
  end
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  arr = []
  (1..num).each do |i|
    arr << i if num % i == 0
  end
  arr.sort
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  arr = []
  factors(num).each do |i|
    arr << i if prime?(i)
  end
  arr
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  even = 0
  odd = 0
  arr.each do |num|
    if num.even?
      even += 1
    else
      odd += 1
    end
  end
  result = []
  if even == 1
    arr.each do |num|
      result << num if num.even?
    end
  else
    arr.each do |num|
      result << num if num.odd?
    end
  end
  result[0]      
end
